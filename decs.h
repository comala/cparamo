//
//  decs.h
//  
//
//  Created by Jesús Rueda-Becerril on 12/8/20.
//

#ifndef decs_h
#define decs_h

// C headers
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <complex.h>

// HDF5
#include "hdf5.h"

// Local headers
#include "const.h"
#include "h5_inout.h"

#endif /* decs_h */
