//=============================================================================
//  const.h
//  FPsolver
//
//  Created by Jesús Rueda-Becerril on 9/28/20.
//=============================================================================

#ifndef const_h
#define const_h

//MARK: MATH
const double PI = 3.141592653589793238462643;
const double twoPI = 6.283185307179586476925287;
const double sqrtPI = 1.772453850905516027298167;
const double sqrtTWO = 1.414213562373095048801689;
const double zero200 = 1.0e-200;
const double half = 5.0e-1;

//MARK: PHYSICS
const double cLight = 2.99792458e10;           // [cm / s]
const double mass_p = 1.67262192369e-24;       // [g]
const double mass_e = 9.1093837015e-28;        // [g]
const double eCharge = 4.80320467299766e-10;   // [cm^(3/2) g^(1/2) / s]
const double sigmaT = 6.6524587321000005e-25;  // [1 / cm^2]
const double hPlanck = 6.62607015e-27;         // [erg s]
const double hbar = 1.0545718176461565e-27;    // [erg s]
const double kBoltz = 1.380649e-16;            // [erg / K]
const double sigmaSB = 5.6703744191844314e-5;  // [erg / cm^2 / K^4 / s]
const double Ggrav = 6.674299999999999e-8;     // [c^3 / g / s^2]
const double eVolt = 1.602176634e-12;          // [erg]
const double nuconst = 2.799248987233304e6;    // eCharge / (2 * pi * m_e * cLight)
const double jconst = 6.6645698196351e-30;     // sqrt(3) * eCharge**2 / (2 * cLight)
const double aconst = 3.6580794255805012e-3;   // sqrt(3) * eCharge**2 / (4 * m_e * cLight)
const double Bcritical = 4.414005218694872e13; // m_e**2 * cLight**3 / (eCharge * hbar)
const double energy_e = 8.187105776823886e-7;  // m_e cLight^2
const double energy_p = 1.5032776159851257e-3; // m_p cLight^2
const double mec2_h = 1.235589963807414e20;    // m_e c^2 / h
const double h_mec2 = 6.53745089363765e-21;    // h / m_e c^2

//MARK: ASTRO
const double AU = 1495978707e13;               // [cm]
const double sunMass = 1.9884754153381438e33;  // [g]
const double sunRadius = 6.957e10;             // [cm]
const double sunLum = 3.828e33;                // [erg / s]
const double parsec = 3.085677581467192e18;    // [cm]
const double jansky = 1e-23;                   // [erg / s cm^2 Hz]
const double lightyr = 9.46073e17;             // [cm]

#endif /* const_h */
