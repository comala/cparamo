//=============================================================================
//  defs.h
//  FPsolver
//
//  Created by Jesús Rueda-Becerril on 9/28/20.
//=============================================================================

#ifndef defs_h
#define defs_h


//MARK: PARAMETERS
#define NUMT (300)
#define NUMG (128)
#define NUMF (192)

//MARK: FUNCTIONS
void an_error(char* error_text);

//double *vector(long nl, long nh);
//void free_vector(double *v, long nl, long nh);

void tridag(double *a, double *b, double *c, double *r, double *u);

void solveFP_cool(double dt, const std::vector<double>& g, const std::vector<double>& nin, const std::vector<double>& dotg, const std::vector<double>& Q, double tesc, std::vector<double>& nout);

#endif /* defs_h */
