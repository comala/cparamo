//=============================================================================
//  h5_inout.cpp
//  FPsolver
//
//  Created by Jesús Rueda-Becerril on 9/30/20.
//=============================================================================

// C++ headers
#include <iostream>
#include <cmath>
#include <vector>
#include <cstdio>
#include <string>
// HDF5 headers
#include "H5Cpp.h"
using namespace H5;
// Local headers
#include "h5_inout.h"

int h5io_WDset(char* hfname, char* dname, int RANK, double data[]) {

    // Try block to detect exceptions raised by any of the calls inside it
    try {
//        H5std_string FILE_NAME(hfname);
//        H5std_string DATASET_NAME(dname);
        std::cout << RANK << std::endl;
        // Turn off the auto-printing when failure occurs so that we can
        // handle the errors appropriately
        Exception::dontPrint();

        // Create a new file using the default property lists.
        H5File file(hfname, H5F_ACC_TRUNC);

        // Create the data space for the dataset.
        hsize_t dims[RANK];               // dataset dimensions
        hsize_t DIM1 = sizeof(data) / sizeof(data[0]);
        dims[0] = DIM1;
        DataSpace dataspace(RANK, dims);

        // Create the dataset.
        DataSet dataset = file.createDataSet(dname, PredType::STD_I32BE, dataspace);

    } catch(FileIException error) {
        // catch failure caused by the H5File operations
        error.printErrorStack();
        return -1;
    } catch(DataSetIException error) {
        // catch failure caused by the DataSet operations
        error.printErrorStack();
        return -1;
    } catch(DataSpaceIException error) {
        // catch failure caused by the DataSpace operations
        error.printErrorStack();
        return -1;
    }
    return 0;  // successfully terminated

}
