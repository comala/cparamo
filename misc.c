//
//  misc.cpp
//  FPsolver
//
//  Created by Jesús Rueda-Becerril on 9/28/20.
//
#include "decs.h"
#include "defs.h"

#define NR_END 1
#define FREE_ARG char*

//MARK: Error handling
void an_error(char error_text[]) {
    //  Description:
    //    Error handler
    fprintf(stderr, "run-time error...\n%s\n...now exiting to system...",error_text);
    exit(1);
}

//MARK: Vector allocation
//double *vector(long nl, long nh) {
//    //  Description:
//    //    Allocate a float vector with subscript range v[nl..nh]
//    double *v;
//    v = (double *)malloc((size_t)((nh-nl+1+NR_END)*sizeof(double)));
//    if (!v) an_error((char*)"allocation failure in vector()");
//    return v-nl+NR_END;
//}
//
//void free_vector(double *v, long nl, long nh) {
//    //  Description:
//    //    Free a float vector allocated with vector()
//    free((FREE_ARG) (v+nl-NR_END));
//}

//MARK: Tridiagonal Algorithm
void tridag(double *a, double *b, double *c, double *r, double *u) {
    //  Description:
    //    Solves the tridiagonal set of linear equations.
    int n = sizeof(r);
    double bet;
    double gam[n][];

    if (b[0] == 0.0) an_error((char*)"Error 1 in tridag");
    
    u[0] = r[0] / (bet=b[0]);
    for (long j=1; j<n; j++) {
        gam[j] = c[j-1] / bet;
        bet = b[j] - a[j] * gam[j];
        if (bet == 0.0) an_error((char*)"Error 2 in tridag");
        u[j] = (r[j] - a[j] * u[j-1]) / bet;
    }
    for (long j=(n-2); j>=0; j--) {
        u[j] -= gam[j+1] * u[j+1];
    }
}

//void numfname(int num){
//    
//}
