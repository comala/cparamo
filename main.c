/******************************************************************************/
//  main.cpp
//  FPsolver
//
//  Created by Jesús Rueda-Becerril on 9/28/20.
/******************************************************************************/

#include "decs.h"
#include "defs.h"


int testing_tridag() {
  int  n = 4;
  double a[] = {0, -1, -1, -1};
  double b[] = {4, 4, 4, 4};
  double c[] = {-1, -1, -1, 0};
  double r[] = {5, 5, 10, 23};
  double u[n];
  // results    {2,  3,  5, 7}

  tridag(a, b, c, r, u);
  for (int i = 0; i < n; i++) {
    fprintf(stdout, "%5g", u[i]);
  }

  fprintf(stdout, "\nn = %5d\nn is not changed hooray !!", n);
  return 0;
}

//int testing_FPsolver() {
//    char number[3];
//    double g1=1.e2, g2=1.e6;
//    double gmin=1.01, gmax=1.5*g2;
//    double tstep=1.e0, tmax=1.e7, pIndex=0.0, B=1.0;
//    double uB, t[NUMT], tacc, tesc, dt;
//    double numDens[NUMG][NUMT];
//    std::string fname;
//    std::vector<double> n1(NUMG, 0.0), n2(NUMG, 0.0), Q0(NUMG, 0.0), C0(NUMG, 0.0), g(NUMG, 1.0);
//
//    fname.assign("FPsolution");
//    fname.append("_t");
//    FILE *myFile;
//    myFile = std::fopen(fname.c_str(), "w");
//
//    uB = pow(B, 2) / (8.0 * PI);
//    t[0] = 0.0;
//
//    for (long k=0; k<NUMG; k++) {
//        g[k] = gmin * pow((gmax / gmin), (double)(k) / (double)(NUMG-1));
//    }
//
//    t[0] = 0.0;
//    for (long k=0; k<NUMG; k++) {
//        if (g[k] >= g1 && g[k] <= g2) {
//            n2[k] = pow(g[k], -pIndex);
//        } else {
//            n2[k] = 0.0;
//        }
//        C0[k] = 3.48e-11;
//    }
//
//    std::cout << C0[0];
//    tacc = 1.0 / (C0[0] * pow(10.0, 4.5));
//    tesc = tacc;
//
//    for (long  i=1; i<NUMT; i++) {
//        t[i] = tstep * pow(tmax / tstep, (double)(i) / (double)(NUMT-1));
//        dt = t[i] - t[i-1];
//
//        for (long k=0; k<NUMG; k++) {
//            numDens[k][i-1] = n2[k];
//            if (t[i] <= tacc) {
//                if (g[k] >= g1 && g[k] <= g2) {
//                    Q0[k] = pow(g[k], -pIndex);
//                } else {
//                    Q0[k] = 0.0;
//                }
//            } else {
//                Q0[k] = 0.0;
//            }
//        }
//        solveFP_cool(dt, g, n1, C0, Q0, 1.e200, n2);
//    }
//    return 0;
//}

int main() {
  int i1 = 5;
  int i2 = 50;
  int i3 = 500;
  double a[1] = {1.0,};

  testing_tridag();
  testing_FPsolver();
  return 0;
}
