USEMPI=0
USEOMP=0
USEHDF5=1

ifeq ($(USEMPI),1)
	CC=mpicc
else  #####  MPI
	ifeq ($(USEHDF5),1)
		CC=h5cc# -std=gnu++11 -march=x86-64
		UH5=-DWITHDF5
	else
		CC=gcc
	endif
endif #####  MPI


# ----- optimization level -----
ifeq ($(DBG),1)

	FLAGS1=-g -Wall -Wextra -Wdeprecated

	ifeq ($(USEOMP),1)
		FLAGS=$(FLAGS1) -openmp $(UH5)
	else
		FLAGS=$(FLAGS1)
	endif

else

	FLAGS1=-g -O3 -Wall -Wextra -Wdeprecated

	ifeq ($(USEOMP),1)
		FLAGS=$(FLAGS1) -openmp $(UH5)
	else
		FLAGS=$(FLAGS1)
	endif
endif

COPT=-c $(FLAGS)
LOPT=$(FLAGS)


# -----  executables  -----
FPSOLV=xFPsolver

# -----  dependencies  -----
FPSOLV_OBJ=misc.o h5_inout.o FPsolver.o main.o

FPSOLV_INC=const.h defs.h h5_inout.h

# -----  rules  -----
all: $(FPSOLV)

# -----  objects deps  -----
misc.o FPsolver.o: const.h defs.h
main.o: FPsolver.o misc.o h5_inout.o h5_inout.h const.h defs.h

# ----- executables rules -----
$(FPSOLV): $(FPSOLV_OBJ) $(FPSOLV_INC)
	$(CC) $(LOPT) $^ -o $@

# -----  objects rules  -----
#%.o: %.h
#	$(CXX) $(COPT) $< -o $@

%.o: %.c $(FPSOLV_INC)
	$(CC) $(COPT) $< -o $@

# -----  PHONY things  -----
.PHONY: clean

clean:
	rm -vf *.o *.il *~
	rm -rvf x*
