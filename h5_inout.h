//
//  h5_inout.h
//  FPsolver
//
//  Created by Jesús Rueda-Becerril on 10/1/20.
//

#ifndef h5_inout_h
#define h5_inout_h

#include "hdf5.h"

using namespace H5;

int h5io_WDset(char* hfname, char* dname, int RANK, double data[]);

#endif /* h5_inout_h */
